﻿using System.Xml.Linq;
using Pierbridge.Connectors.Classes;

namespace SmartFreight
{
    public class Settings : SettingsBase
    {
        public Settings(XElement requestElement, XElement settingsElement)
            : base(requestElement, settingsElement)
        {}

        // Available settings. This list can be expanded as we identify other settings necessary to properly integrate to SmartFreight
        // Please note, a username, password, and/or api key if needed comes in with the actual inbound Pierbridge XML and does not require separate settings

        public string Uri
        {
            get { return Live ? base.GetString("smartfreight_uri_live") : base.GetString("smartfreight_uri_test"); }
        }
    }
}
