﻿using System;
using System.Xml.Linq;
using Pierbridge.Connectors.Classes;

namespace SmartFreight
{
    [Connector("SmartFreight")]
    public class Connector : ConnectorBase
    {
        protected XDocument requestDoc { get; private set; }

        public new Settings Settings
        {
            get
            {
                return (Settings)settings;
            }
        }

        public Connector(string xmlRequest, string xmlSettings)
            : base(xmlRequest, xmlSettings)
        {
            // This code loads the inbound pierbridge XML string into an XDocument.  This initialization can be replaced as needed
            requestDoc = XDocument.Parse(xmlRequest);
            
        }

        // Suggested uses:

        // Add a method to communicate to SmartFreight API here??

        // If necessary, add a method to build authentication or other common information across functions here??

        // If necessary, add a method to handle common error scenarios across functions here??

        protected override void InitialiseSettings(string xmlRequest, string xmlSettings)
        {
            base.settings = new Settings(XElement.Parse(xmlRequest), XElement.Parse(xmlSettings));
        }

        /// <summary>
        /// Process function
        /// </summary>
        /// <returns></returns>
        public override string Process()
        {
            throw new NotImplementedException();
        }
    }
}
