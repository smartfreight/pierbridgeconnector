﻿using System;
using Pierbridge.Connectors.Classes.PierbridgeObjects;

namespace SmartFreight
{
    [Function(Pierbridge.Connectors.Enums.Functions.Ship)]
    public class FunctionShip : Connector
    {
        public FunctionShip(string xmlRequest, string settings)
            : base(xmlRequest, settings)
        {}

        public override string Process()
        {
            string response = string.Empty;

            try
            {
                // Logic to transform Pierbridge ship request XML to SmartFreight ship request message

                // Logic to validate request data if need be prior to sending request (return error to user if need be)

                // Logic to send transaction to SmartFreight and receive response

                // Logic to transform SmartFreight ship response message to Pierbridge ship response XML

                // Logic to validate response data if need be prior to returning response (return error to user if need be)
            }
            catch (Exception ex)
            {
                base.HandleFunctionException(ex);
                response = new PierbridgeBasicError(ex.Message, FunctionType).Xml;
            }

            return response;
        }
    }
}
